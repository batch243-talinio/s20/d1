// console.log("ZAWARUDO1 !");

// [Section] While Loop

	// while loop takes expression/condition.

	// expressions are unit of code that can be evaluated to a value.
	// if the condition evaluates to be true, the statements inside the code block will be executed.

	// a loop will iterate a certain number if times until an expression/condition is met.
	
	/*
		Syntax:
			while(condition){
				statements;
				iteration;
			}
	*/

//	let count = 5;

//	while(count !== 0){
//		console.log("While: " + count);
//
//		count--; // count - 1 for every iteration until "while condition" is met (til count drops to zero).
//	}

	// BE CAREFUL! Loop statements occupies a significant amount of memory space in our devices when it enters an infinite loop.

// [Section] Do While Loop

	// a do while-loop works a lot like while loop with an exception of executing atleast 1 statement before checking conditions wether the condition woulb result to true or false

	/*
		Syntax:
			do{
				statement;
				iteration;
			}while(expression/condition);
	*/

/*
	let number = Number(prompt("Give me a number: ")); // could use parseInt to change string to Int as well.

	do{
		console.log("Do while: " + number);
		number++;
	}while(number < 10);
*/

// [Section] For Loop
	// for loop is more flexible than while and do-while
	/*
		it consists of 3 parts:
			1. initialization - val that will track the progress of the loop.
			2. expression - will be the one to determine whether the loop will continue.
			3. finalExpression - indicates how to advance the loop.
	*/

	/*
	Syntax:
		for(initialization; expresion/condition; finalExpression){
			statement;
		}
	*/

/*	for(let count = 0; count <= 20; count++){
		console.log("For loop: " + count);
	}*/

let myString = "alex";
	// characters on a string may be be counted using the .lenght property.
	// strings are special compare to other data types that it has access to functions and other pieces of information.

	console.log(myString.length);

	// accessing characters of a string
	// Individual characters of a string may be access using its index number.
	// the first char in a string corresponds to 0, the next is 1 up to the nth/last character.

	//console.log(myString[0]);
	//console.log(myString[1]);
	//console.log(myString[2]);
	//console.log(myString[3]);

	/*
	for(let i = 0; i < myString.length; i++){
		console.log(myString[i]);
	}

	let numA = 15;

	for(let x = 0; x <= 10 ; x++){
		let exponential = numA ** x;
		console.log(exponential);
	}*/

	// create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is vowel.

	let myName = "alex";

	for(let i = 0; i < myName.length; i++){
		if(myName[i].toLowerCase() === 'a' || 
			myName[i].toLowerCase() === 'e' ||
			myName[i].toLowerCase() === 'i' ||
			myName[i].toLowerCase() === 'o' ||
			myName[i].toLowerCase() === 'u'){

			console.log(3);
		}else{
			console.log(myName[i]);
		}
	}

// [Section] Continue and Break Statements

/*
	- continue statements allows code to go to the next iteration of the loop w/o finishing the execution of all statements in a code block.
	- break statement is used to terminate the current loop once a mathc has been found.
*/


	/*
	Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
	    - How this For Loop works:
	        1. The loop will start at 0 for the the value of "count".
	        2. It will check if "count" is less than the or equal to 20.
	        3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
	        4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
	        5. If the value of count is not equal to 0, the console will print the value of "count".
	        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
	        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
	        8. The value of "count" will be incremented by 1 (e.g. count = 1)
	        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement

	*/

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0;
	if(count % 2 == 0){

		// tells the code to continue to the next iteration of the loop.
		// this ignores all statements located after  the continue statement.
		continue;
	}else if(count > 10){
		//tells the code to terminate the loop even if the condition of the loopp defines that it should execute so long as the value of count is ...
		break;
	}
	console.log("Contine and Break: " + count);
}


let noName = "alexandro";

for(let i = 0; i < noName.length; i++){
	if(noName[i] === 'a'){
		console.log("Continue to next iteration.");
		constinue;
	}

	if(noName[i] === 'd'){
		console.log("Console log before the break.");
		break;
	}
	console.log(noName[i]);
}